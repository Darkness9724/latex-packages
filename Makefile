INSTALL=install -m 644 $@ ${TEXMFHOME}/tex/latex/local/$@

build: darkscience.sty fancytitle.sty komamisc.sty hyperrus.sty

darkscience.sty:
	$(INSTALL)

fancytitle.sty:
	$(INSTALL)

komamisc.sty:
	$(INSTALL)

hyperrus.sty:
	$(INSTALL)
